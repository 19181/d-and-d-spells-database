const nodegui = require("@nodegui/nodegui");
const sqlite3 = require("sqlite3");
const {Worker} = require("worker_threads");

const numberSuffixes = {1: "1st", 2: "2nd", 3: "3rd", 4: "4th", 5: "5th", 6: "6th", 7: "7th", 8: "8th", 9: "9th"};
const characterClasses = ["bard", "cleric", "druid", "fighter", "paladin", "ranger", "rogue", "sorcerer", "warlock", "wizard"];
const spellSchools = ["Abjuration", "Conjuration", "Divination", "Enchantment", "Evocation", "Illusion", "Necromancy", "Transmutation"];

var db = new sqlite3.Database("database.db", err => {
	if(err) {
		console.log("Couldn't connect to database: " + err);
		process.exit();
	}
});

const win = new nodegui.QMainWindow();
win.resize(1000, 800);
//win.showMaximized();
win.setWindowTitle("Spell database");

//+ This main widget will encompass all of the other elements
var centralWidget = new nodegui.QWidget();
win.setCentralWidget(centralWidget);
centralWidget.setLayout(new nodegui.FlexLayout());
centralWidget.setObjectName("centralWidget");

//+ Contains the elements that are used to search for a spell
var spellControlsFieldset = new nodegui.QWidget();
spellControlsFieldset.setLayout(new nodegui.FlexLayout());
centralWidget.layout.addWidget(spellControlsFieldset);
spellControlsFieldset.setObjectName("controlsFieldset");


//+/// SECTION 1 - spell name ////////////////////////////////////
var heading = new nodegui.QLabel();
heading.setText("Search for a spell:");
heading.setInlineStyle("font: 15pt; margin-bottom: 5px; margin-left: -5px;");
spellControlsFieldset.layout.addWidget(heading);

var input = new nodegui.QLineEdit();
input.setPlaceholderText("Enter search terms here");
input.setObjectName("spellInputBox");
spellControlsFieldset.layout.addWidget(input);
//+//////////////////////////////////////////////


//+////// a container for sections 2-5 /////////////////////////////
var subFieldset1 = new nodegui.QWidget();
subFieldset1.setLayout(new nodegui.FlexLayout());
subFieldset1.setInlineStyle("flex-direction: row;");
spellControlsFieldset.layout.addWidget(subFieldset1);
//+/////////////////////////////////////////////////////////


//+////////// SECTION 2 - spell level //////////////////////
var spellLevelSelection = new nodegui.QWidget();
spellLevelSelection.setLayout(new nodegui.FlexLayout());
spellLevelSelection.setObjectName("spellPropertySelection");
spellLevelSelection.setInlineStyle("flex-direction: column;");
subFieldset1.layout.addWidget(spellLevelSelection);

var minLevelLabel = new nodegui.QLabel();
minLevelLabel.setText("Minimum level:");
spellLevelSelection.layout.addWidget(minLevelLabel);

var minLevel = new nodegui.QSpinBox();
minLevel.setMinimum(0);
minLevel.setMaximum(9);
minLevel.setInlineStyle("width: 100px;")
spellLevelSelection.layout.addWidget(minLevel);

var maxLevelLabel = new nodegui.QLabel();
maxLevelLabel.setText("Maximum level:");
spellLevelSelection.layout.addWidget(maxLevelLabel);

var maxLevel = new nodegui.QSpinBox();
maxLevel.setMinimum(0);
maxLevel.setMaximum(9);
maxLevel.setValue(9);
maxLevel.setInlineStyle("width: 100px;")
spellLevelSelection.layout.addWidget(maxLevel);
//+//////////////////////////////////////////////////////////////////


//+////////// SECTION 3 - spell components ///////////////////////
var spellComponentsSelection = new nodegui.QWidget();
spellComponentsSelection.setLayout(new nodegui.FlexLayout());
spellComponentsSelection.setInlineStyle("margin-left: 10px; flex-direction: column;");
spellComponentsSelection.setObjectName("spellPropertySelection");
subFieldset1.layout.addWidget(spellComponentsSelection);

var componentsLabel = new nodegui.QLabel();
componentsLabel.setText("Components:");
spellComponentsSelection.layout.addWidget(componentsLabel);

//? Make 3 checkboxes for the spell components
var componentsCheckboxes = [];
for(var i = 0; i < 3; i++) {
    let checkbox = new nodegui.QCheckBox();
    //? Formula to convert 0, 1 and 2 to the ASCII codes for "V", "S" and "M". Quadratic regression.
    checkbox.setText(String.fromCharCode(86 - 1.5*i - 1.5 * i**2));
    componentsCheckboxes.push(checkbox);
    spellComponentsSelection.layout.addWidget(componentsCheckboxes[i]);
}
//+/////////////////////////////////////////////////////////////////////


//+////////// SECTION 4 - spell schools //////////////////////////
var spellSchoolSelection = new nodegui.QWidget();
spellSchoolSelection.setLayout(new nodegui.FlexLayout());
spellSchoolSelection.setInlineStyle("margin-left: 10px; flex-direction: row;");
spellSchoolSelection.setObjectName("spellPropertySelection");
subFieldset1.layout.addWidget(spellSchoolSelection);

var schoolCheckboxes = [];

//+////////// SECTION 4a ////////////////////////////////
var spellSchoolSelectionA = new nodegui.QWidget();
spellSchoolSelectionA.setLayout(new nodegui.FlexLayout());
spellSchoolSelection.layout.addWidget(spellSchoolSelectionA);

var schoolLabel = new nodegui.QLabel();
schoolLabel.setText("Select spell schools:");
spellSchoolSelectionA.layout.addWidget(schoolLabel);

for(var i = 0; i < spellSchools.length / 2; i++) {
    let checkbox = new nodegui.QCheckBox();
    checkbox.setText(spellSchools[i]);
    schoolCheckboxes.push(checkbox);
    spellSchoolSelectionA.layout.addWidget(schoolCheckboxes[i]);
}
//+///////////////////////////////////////////////

//+///////// SECTION 4b //////////////////////////
var spellSchoolSelectionB = new nodegui.QWidget();
spellSchoolSelectionB.setLayout(new nodegui.FlexLayout());
spellSchoolSelectionB.setInlineStyle("margin-top: 13px;");
spellSchoolSelection.layout.addWidget(spellSchoolSelectionB);

for(var i = spellSchools.length / 2; i < spellSchools.length; i++) {
    let checkbox = new nodegui.QCheckBox();
    checkbox.setText(spellSchools[i]);
    schoolCheckboxes.push(checkbox);
    spellSchoolSelectionB.layout.addWidget(schoolCheckboxes[i]);
}
//+//////////////////////////////////////////////
//+/////////////////////////////////////////////////////////////////////


//+///// SECTION 5 - character classes that can cast the spell ////////////////////
var characterClassSelection = new nodegui.QWidget();
characterClassSelection.setLayout(new nodegui.FlexLayout());
characterClassSelection.setInlineStyle("margin-left: 10px; flex-direction: row;");
characterClassSelection.setObjectName("spellPropertySelection");
subFieldset1.layout.addWidget(characterClassSelection);

var classCheckboxes = [];

//+//////// SECTION 5a //////////////////////////////
var characterClassSelectionA = new nodegui.QWidget();
characterClassSelectionA.setLayout(new nodegui.FlexLayout());
characterClassSelection.layout.addWidget(characterClassSelectionA);

var classLabel = new nodegui.QLabel();
classLabel.setText("Select character classes:");
characterClassSelectionA.layout.addWidget(classLabel);

for(var i = 0; i < characterClasses.length / 2; i++) {
    let checkbox = new nodegui.QCheckBox();

    //+ Capitalize the string
    let c = characterClasses[i];
    checkbox.setText(c.charAt(0).toUpperCase() + c.slice(1));
    
    classCheckboxes.push(checkbox);
    characterClassSelectionA.layout.addWidget(classCheckboxes[i]);
}
//+/////////////////////////////////////////////////////

//+////// SECTION 5b ///////////////////////////////////
var characterClassSelectionB = new nodegui.QWidget();
characterClassSelectionB.setLayout(new nodegui.FlexLayout());
characterClassSelectionB.setInlineStyle("margin-top: 13px;");
characterClassSelection.layout.addWidget(characterClassSelectionB);

for(var i = characterClasses.length / 2; i < characterClasses.length; i++) {
    let checkbox = new nodegui.QCheckBox();

    //+ Capitalize the string
    let c = characterClasses[i];
    checkbox.setText(c.charAt(0).toUpperCase() + c.slice(1));
    
    classCheckboxes.push(checkbox);
    characterClassSelectionB.layout.addWidget(classCheckboxes[i]);
}
//+/////////////////////////////////////////////////////
//+///////////////////////////////////////////////////////////////////////////////


//+ SECTION 6 - code that starts the spell search ///////////////////////////
var goButton = new nodegui.QPushButton();
goButton.setText("Go");
goButton.setInlineStyle("margin-top: 5px; height: 50px; font: 12pt;");
goButton.addEventListener("pressed", () => queryDatabase());
spellControlsFieldset.layout.addWidget(goButton);

//+ This tracks when the enter key is pressed. It will be the same as clicking the go button
win.addEventListener(nodegui.WidgetEventTypes.KeyRelease, event => {
    let keyEvent = new nodegui.QKeyEvent(event);
    if(keyEvent.key() == nodegui.Key.Key_Return)
        queryDatabase();
});
//+////////////////////////////////////////////////////////////////////////////


//+ This scroll area will hold the output of the search
var outputScrollArea = new nodegui.QScrollArea();
outputScrollArea.setObjectName("scrollArea");
centralWidget.layout.addWidget(outputScrollArea);

//+ This button will open a popup when clicked. The popup will show the user the SQL of the last query
var SQLButton = new nodegui.QPushButton();
SQLButton.setText("Show SQL");
SQLButton.setInlineStyle("margin-top: 5px; width: 60px; padding: 2px;");
SQLButton.addEventListener("pressed", () => {
    let SQLObj = createSQL();
    //+ Put the parameters of the query into the sql display string
    let text = SQLObj.SQL;
    let paramIndex = 0;
    for(var i = 0; i < text.length; i++) {
        if(text[i] == "?") {
            text = text.slice(0, i) + SQLObj.SQLParams[paramIndex] + text.slice(i + 1);
            paramIndex++;
        }
    }

    SQLMessageBox.setText(`The sql for the current query is:\n\n${text}`);
    SQLMessageBox.exec();
});
centralWidget.layout.addWidget(SQLButton);

//-   ↕

var SQLMessageBox = new nodegui.QMessageBox();
SQLMessageBox.setWindowTitle("SQL");
var SQLMessageBoxButton = new nodegui.QPushButton();
SQLMessageBoxButton.setText("OK");
SQLMessageBox.addButton(SQLMessageBoxButton, nodegui.ButtonRole.AcceptRole);

centralWidget.setStyleSheet(`
#centralWidget {
    padding: 10px;
}

#controlsFieldset {
    border: 1px solid black;
    padding: 15px;
    flex: 0 0;
}

#spellPropertySelection {
    border: 1px solid black;
    margin-top: 5px;
    padding: 10px;
}

#scrollArea {
    flex: 1 1;
    margin-top: 15px;
}
`);

win.show();
//! Stops the window from being garbage collected after a few minutes
global.win = win;

//+ Creates and returns the SQL query
function createSQL() {
    //+ This is the SQL that will be run. We are selecting the spells that are part of the character classes that the user selects.
    //+ Also joining the spell schools table to the spells table
    let SQL = "SELECT spells.*, spell_schools.name AS spell_school_name, classes.name AS classes FROM spells INNER JOIN 'character-spell_links' csl ON csl.spell_id = spells.id INNER JOIN (SELECT csl.spell_id, group_concat(character_classes.name, ', ') AS name FROM character_classes INNER JOIN 'character-spell_links' csl ON csl.character_class_id = character_classes.id GROUP BY csl.spell_id) classes ON spells.id = classes.spell_id INNER JOIN spell_schools ON spells.school_id = spell_schools.id WHERE spells.name LIKE ? AND spells.level >= ? AND spells.level <= ?";
    //+ And these will be the paramters for the query
    let SQLParams = [`%${input.text()}%`, minLevel.value(), maxLevel.value()];

    //+ Get the right numbers for the v, s and m columns
    let componentBools = [];
    let allowAnyComponentCombo = true;
    for(var i = 0; i < 3; i++) {
        if(componentsCheckboxes[i].isChecked()) {
            componentBools[i] = 1;
            allowAnyComponentCombo = false;
        }
        else
            componentBools[i] = 0;
    }

    //+ If at least one component checkbox was checked then we need to add the stuffs to the SQL query
    if(!allowAnyComponentCombo)
        SQL += ` AND spells.v_component = ${componentBools[0]} AND spells.s_component = ${componentBools[1]} AND spells.m_component = ${componentBools[2]}`;

    //+ At the start we will need to add and AND to the query
    SQL += " AND (";
    for(var i = 0; i < spellSchools.length; i++) {
        //+ If we have a checked check box then we can add it to the query
        if(schoolCheckboxes[i].isChecked()) {
            if(!SQL.endsWith("(")) SQL += " OR";
            SQL += " spell_schools.id = ?";
            SQLParams.push(i + 1);
        }

        //+ If this is the last checkbox and the sql string currently ends with a "(", then no checkboxes were checked. We add an always true statement to display every spell school
        if(i == spellSchools.length - 1 && SQL.endsWith("("))
            SQL += "1=1";
    }

    SQL += ") AND (";

    for(var i = 0; i < characterClasses.length; i++) {
        //+ If we have a checked check box then we can add it to the query
        if(classCheckboxes[i].isChecked()) {
            if(!SQL.endsWith("(")) SQL += " OR";
            SQL += " csl.character_class_id = ?";
            SQLParams.push(i + 1);
        }

        //+ If this is the last checkbox and the sql string currently ends with a "(", then no checkboxes were checked. We add an always true statement to display every c-class
        if(i == spellSchools.length - 1 && SQL.endsWith("("))
            SQL += "1=1";
    }
    SQL += ") GROUP BY spells.id";

    return {SQL: SQL, SQLParams: SQLParams};
}

//+ Queries the database based on the input parameters and makes a spell-output container of the returned spell data
function queryDatabase() {
    let statusLabel = new nodegui.QLabel();
    statusLabel.setInlineStyle("font: 25pt;");
    statusLabel.setAlignment(nodegui.AlignmentFlag.AlignCenter);
    
    //+ Make sure that the levels arent messed up
    if(minLevel.value() > maxLevel.value()) {
        statusLabel.setText("The minimum level cannot be bigger than the maximum level!");
        updateScrollArea(statusLabel);
        return;
    }
    else {
        statusLabel.setText("Searching...");
        updateScrollArea(statusLabel);
    }

    let SQLObj = createSQL();
    console.log("\nRunning the following SQL:\n\n" + SQLObj.SQL + "\n\nWith the following parameters: \n\n" + SQLObj.SQLParams + "\n");

    //+ Make a statement to protect against SQL injection
    let sqlStatement = db.prepare(SQLObj.SQL);
    sqlStatement.all(SQLObj.SQLParams, (err, rows) => {
        if(err)
            console.log("Error selecting data: " + err);

        else if(rows.length < 1) {
            statusLabel.setText("No results");
            updateScrollArea(statusLabel);
        }

        else {
            //+ We are done searching, we are now loading the spell card widgets into the container
            statusLabel.setText("Loading...");
            updateScrollArea(statusLabel);

            let container = new nodegui.QWidget();
            container.setLayout(new nodegui.FlexLayout());
            container.setInlineStyle("padding: 5px; flex-direction: column;");

            //+ The name explains it, this nested function will add a spell to the spells container
            function addSpellCardToContainer(i) {
                //+ When all of the spells have been added, add the container to the scroll area
                if(i == rows.length) {
                    updateScrollArea(container);
                    return;
                }

                container.layout.addWidget(createSpellCard(rows[i]));

                //+ Recursively call this function after the event loop has been processed. This doesn't make the application freeze while the results are loaded
                setImmediate(addSpellCardToContainer.bind(null, i + 1));
            }

            //? Start the sqeuence
            addSpellCardToContainer(0);
        }
    });
}

//+ Returns a widget that can be displayed on the window. The widget is created so that it is in the shape of spell card, with the important information
function createSpellCard(spell) {
    //+ Main outline of the spell //////
    let card = new nodegui.QWidget();
    card.setObjectName("mainOutline");
    card.setLayout(new nodegui.FlexLayout());

    //+ Name of spell
    let name = new nodegui.QLabel();
    name.setObjectName("name");
    name.setText(spell.name);
    card.layout.addWidget(name);
    //+////////////////

    //+ Level and school ///
    let details = new nodegui.QLabel();
    let detailsString = spell.level == 0 ? `${spell.spell_school_name} cantrip` : `${numberSuffixes[spell.level]} level ${spell.spell_school_name}`;
    details.setText(detailsString);
    details.setObjectName("details");
    card.layout.addWidget(details);
    //+/////////////////////

    //+ Casting time, range, duration, components and materials container
    let stats = new nodegui.QLabel();
    stats.setObjectName("stats");
    stats.setWordWrap(true);

    let materialsString = spell.materials == "NULL" ? "" : `\nMaterials: ${spell.materials}`;
    let componentsString = "";

    if(spell.v_component == 1) componentsString += "V";
    if(spell.s_component == 1) componentsString += "S";
    if(spell.m_component == 1) componentsString += "M";
    componentsString = componentsString.split("").join(", ");

    stats.setText(`Casting time: ${spell.cast_time}\nRange: ${spell.range}\nDuration: ${spell.duration}\nComponents: ${componentsString}${materialsString}`);
    card.layout.addWidget(stats);
    //+///////////////////////////////////////////////////////

    //+ Classes that can cast the spell
    let classes = new nodegui.QLabel();
    classes.setText(spell.classes);
    classes.setInlineStyle("margin-top: 5px;");
    card.layout.addWidget(classes);
    //+/////////////////////////////

    //+////////////////////////////////////////
    let description = new nodegui.QLabel();
    description.setText(spell.description.replace(/•/g, "\n•"));
    description.setObjectName("description");
    description.setWordWrap(true);
    card.layout.addWidget(description);
    //+//////////////////////////////////////////

    card.setStyleSheet(`
    #mainOutline {
        border: 1px solid rgb(255, 96, 0);
        flex-direction: column;
        width: 500px;
        padding: 15px;
        margin-bottom: 15px;
    }

    #name {
        border: 1px solid black;
        padding: 2px;
        flex: 0 0 0;
    }

    #details {
        color: blue;
        margin-top: 4px;
    }

    #stats {
        border: 1px solid red;
        margin-top: 5px;
        padding: 2px;
    }

    #description {
        margin-top: 5px;
        margin-bottom: 5px;
    }
    `);

    return card;
}

//+ Displaying multiple spells takes up space. We need to use a scroll area in order to view all of them. However, everytime the output is updated, we also need to update the widget
//+ for the scroll area. If we don't update it, the scroll area will assume nothing has changed and wont scroll to the new spells
function updateScrollArea(container) {
    //? Remove existing container from the scrollArea
    let oldContainer = outputScrollArea.takeWidget();
    if(oldContainer)
        oldContainer.close();

    outputScrollArea.setWidget(container);
}
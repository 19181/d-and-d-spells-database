@echo off
call npx nodegui-packer --init SpellSearcher
mkdir src
robocopy ./ src *.js nodegui_core.node
echo Packering
call npx nodegui-packer --pack ./src
rmdir /S /Q src
move ./deploy/win32/build/SpellSearcher ./
rmdir /S /Q deploy
copy /B /V /Y database.db SpellSearcher
echo Finished
pause
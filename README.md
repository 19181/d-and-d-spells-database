# SpellSearcher

Yo Mr. P or whoever is marking this. This app searches for D&D spells based on criteria you specify. As you can tell, the name `SpellSearcher` is very, extremely creative and I came up with it all by myself.

## Files

My code is all in the main directory:
- `index.js` is the main app
- `dbCreator.js` makes the database file and creates the tables
- `dbPopulator.js` populates the database with all of the spells
- The spells are pulled from the `.csv` files stored in the `google_sheets` directory
- My database was designed with [SQL designer](https://ondras.zarovi.cz/sql/demo/). You can export the data and save it as xml. The xml for my design is stored in `database-design.xml`
- Rather obviously, the actual database is `database.db`

## Usage

Using my `pack.bat` file I have created a windows executable that you can run so you don't have to compile nodegui yourself. Head on over to the `SpellSearcher` directory, and take a look inside. There are a bunch of dll's and other folders, but you want to be running `qode.exe`. I cannot rename this, because it messes up some other stuff so for the time being it will have to stay named as `qode`. The app will run, and you can test it out.

__Edit:__
I thought it was working, but apparently the app doesn't run on some computers. I tried to recompile it multiple times, but I really don't know what's wrong and to be honest, I cant be bothered tyrying to find out. If this happens to you, just run the source code (see section below).

## Manual installation

If, for some strange reason, you really just want to run the code yourself instead of running the executable, it is possible to do so. Last time I checked, New Zealand was a free country. You will just have to open up a command prompt in the main directory. Make sure you run it as administrator, it's possible for permission problems to occur if you don't. Once that's open, run the command `npm install` (this does assume you have node.js and npm installed of course). This will install all of the dependencies. It may take a while because of nodegui (some c++ stuff needs to compile). Be patient. Go drink a coffee or something. The installation should hopefully finish without erroring out. Once it does, verify that you have the `node_modules` folder. You can then run the command `npm start` to start the app.
const fs = require("fs");
const sqlite3 = require("sqlite3");
const csvParse = require('csv-parse/lib/sync');
const {cursorTo} = require("readline");

const characterClasses = ["bard", "cleric", "druid", "fighter", "paladin", "ranger", "rogue", "sorcerer", "warlock", "wizard"];
const spellSchools = ["Abjuration", "Conjuration", "Divination", "Enchantment", "Evocation", "Illusion", "Necromancy", "Transmutation"];

//+ A map of spell names to id's. We need to look up the spell ID if there is a duplicate entry
var spellIDs = new Object();

var db = new sqlite3.Database("database.db", err => {
	if(err) {
		console.log("Couldn't connect to database: " + err);
		process.exit();
	}
});

//+ Want to run database calls synchronously for the populator
function runSQL(db, sql) {
	return new Promise((resolve, reject) => {
		//console.log("Running " + sql + "\n");
		db.run(sql, err =>  {
			if(err) reject(err);
			resolve();
		});
	});
}

//+ Draws a terrible looking progress bar for the database population
function drawProgressBar(progress) {
    cursorTo(process.stdout, 0, 1);
	process.stdout.write("\x1b[32m\x1b[?25l[                                                  ] \x1b[0m ");
	cursorTo(process.stdout, 1, 1);
	process.stdout.write("\x1b[?25l░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░\x1b[0m");
	cursorTo(process.stdout, 1, 1);
    
    for(var i = 0; i < Math.round(progress * 50); i++)
        process.stdout.write("\x1b[36m\x1b[?25l█\x1b[0m");

    cursorTo(process.stdout, 53, 1);
    process.stdout.write(`\x1b[33m\x1b[?25l${Math.round(progress * 100)}%\x1b[0m`);
}

async function main() {
    console.clear();
    console.log("\x1b[35mInserting spell schools...\x1b[0m");

	//+ Need to add all of the spell schools to the table. These will be linked to the spells table
    let progress = 1 / spellSchools.length;
	for(var i = 0; i < spellSchools.length; i++) {
        drawProgressBar(progress);
        progress += 1 / spellSchools.length;

        await runSQL(db, `INSERT INTO spell_schools (name) VALUES ('${spellSchools[i]}')`);
    }

	//+ Reset stuff
    let spellIndex = 1;
    progress = 0;

    console.clear();
    console.log("\x1b[35mInserting spells...\x1b[0m");

	//+ For every character class we insert its name (capitalized) into the table and parse out the spells
	for(var i = 0; i < characterClasses.length; i++) {
		await runSQL(db, `INSERT INTO character_classes (name) VALUES ('${characterClasses[i].charAt(0).toUpperCase() + characterClasses[i].slice(1)}');`);

		let fileContents = fs.readFileSync(`google_sheets/${characterClasses[i]}.csv`).toString();
		let spells = csvParse(fileContents);

        //+ Go through each line
		for(var lineIndex = 0; lineIndex < spells.length; lineIndex++) {
            progress += 1 / (characterClasses.length * spells.length);
            drawProgressBar(progress);

			let values = spells[lineIndex];
			values.shift();
			
			//-                                                               |----------------------------- ↓
			//+ If there are any single quotes in the text then we need to escape them to make sure they don''t mess with the query
			//-                                                                         see what I did there ↑
			for(var valueIndex = 0; valueIndex < values.length; valueIndex++)
				values[valueIndex] = values[valueIndex].replace(/'/g, "''");

			//+ Get some of the data from the array

			let schoolData = values[2].split(" ");
			let schoolName = schoolData[(schoolData[1] == "cantrip") ? 0 : 2];

			let descriptionString = values[7];
			let materialsString = "NULL";

			//+ Some spells that have an 'M' component don't seem to have specific materials, so we need to check first (look for "(" at start of description)
			if(values[7].startsWith("(") && values[5].includes("M")) {
				materialsString = values[7].split(")")[0].substr(1);
				descriptionString = values[7].substr(values[7].indexOf(")") + 1);
            }
            
            let componentsArray = [];
            for(var c = 0; c < 3; c++)
                //+ Check if the components include V, S or M. For each one we can set a 1 in the array if it is there, or we can set a 0
                componentsArray[c] = values[5].includes(String.fromCharCode(86 - 1.5*c - 1.5 * c**2)) ? 1 : 0;

            let spellID = spellIndex;

            //+ Insert the spell data. The script will exit and inform us if there is an error with the db
			await runSQL(db, `INSERT INTO spells (name, level, school_id, cast_time, range, materials, duration, description, v_component, s_component, m_component)
            VALUES ('${values[1]}', ${parseInt(values[0])}, ${spellSchools.indexOf(schoolName) + 1}, '${values[3]}', '${values[4]}',
            '${materialsString}', '${values[6]}', '${descriptionString}', ${componentsArray[0]}, ${componentsArray[1]}, ${componentsArray[2]})`)
			.catch(err => {
				//? An error code of 19 means an sql constraint error - most likely an issue with the unique key.
				//? This means we have a duplicate entry, so we need to go back and find the correct ID of the spell.
				if(err.errno == 19) {
                    spellID = spellIDs[values[1]];
                    spellIndex--;
				}
				//! Otherwise it will be a normal error
				else {
					console.log(err);
					process.exit();
				}
            });

			//- Now we need to link the spell with the character class, as some spells can be cast by more than one class
			await runSQL(db, `INSERT INTO 'character-spell_links' (character_class_id, spell_id) VALUES (${i + 1}, ${spellID})`)
			.catch(err => {
				console.log(err)
				process.exit();
			});

			//+ Save it in case this spell comes up in another class
			spellIDs[values[1]] = spellID;
            spellIndex++;
        }
	}

	//+ We are done
	db.close();
	console.log("\x1b[?25h\nFinished");
}

main();